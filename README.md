---
title: "Diario IC - 2021"
author: "Caique Sarkis"
description: "Simulações computacionais de fenômenos físicos"
---
# Diário IC
*Autor: Caique Sarkis*

---
---

## Abril 2021
**Todo list geral**

- [ ] Terminar Post NanoDrop
  
- [x] Simulação da página inicial do site do grupo Fisex
  
- [ ] Estudar Surface Evolver
	- [x] Fazer algum exemplo
	- [ ] Fazer algo sozinho

---

### *Semana* **19/04/2021 -25/04/2021**

**Seg** 

: Mudar simulação da página principal de 3D para 2D.

Trabalhei no projeto [lj-box](https://caiquesarkis.gitlab.io/lj-box/), que é a simulação que será mostrada na tela de início do site. Comecei fazendo a transição do render do THREE.JS para o JavaScript básico, onde posso mostrar as imagens com o elemento Html Canvas. Essa mudança, que é aparentemente pequena, mudou completamente o código, uma vez que tive que mudar todos os vetores que descreviam a dinâmica do problema.

```javascript
//Exemplo de vetor antigo
let position = [new THREE.Vector3(0,0,0),new THREE.Vector3(0,0,0)];

//Exemplo de vetor novo
let position = [[0,0],[0,0]];
```

A integração de Verlet, o cálculo de potencial de Lennard Jones e a distância entre as partículas todas eram escritas em termos do outro tipo de vetor, isso gerou problemas em cascata, quando se modificava em um lado do programa tudo era  afetado. Por fim, consegui um resultado razoável, onde tenho a mecânica funcionando normalmente, no entanto, preciso dar uma polida no visual.

---

**Ter**

: Organizar diario de IC/ Trabalhar no visual de [lj-box](https://caiquesarkis.gitlab.io/lj-box/)/ Atualizar info

Criei um repositório Git-Lab para o diario de ic e coloquei na info do site fisexlab

Atualizei a info no site fisexlab com meus horários, meu site e o link para o repo do diário

Modificações no visual do lj-box

* Fiz o background ter a cor da seção Fis.JS
* Ajustei as posições iniciais para que sempre fiquem no meio da tela independete to tamanho da janela
* Fiz o programa se ajustar ao mobile

---

**Quar**

: Estudar surface evolver/continuar mudanças no visual do lj-box

Surface evolver
* Realizei a minimização de uma caixa que se tornou uma esfera 

Ultimas modificações no lj-box

* Adicionei condições de fronteira (velocidade é refletida nas paredes usando integração de Verlet)
* Adicionei cor dinâmica que depende da distância entre os átomos
* Modifiquei algumas constante do potencial para um efeito visual mais agradavel

---

**Qui**

: Estudar obsidian

Obsidian
- Estudei o app através desse [curso](https://www.youtube.com/watch?v=QgbLb6QCK88&list=PL3NaIVgSlAVLHty1-NuvPa9V0b0UwbzBd)

---
**Sex**

: Reescrever post Nanodrop

Escrevi boa parte do novo post

---

### *Semana* **26/04/2021 -03/05/2021**

**Seg** 

: Ajustar parâmetros do lj-box/ pesquisar sobre calculo variacional e método de minimização de energia

- Modifiquei alguma variaveis da dinâmica do lj-box para que as partículas fiquem mais tempo grudadas.

---

**Ter**
: Estudar cálculo variacional

-  Estudei sobre cálculo variacional e deduzi a equação de euler lagrange

---

**Quar**
: Continuar a trabalhar na atualização do post Nano Drop/ modificar o programa nanodrop 


- Escrevi 80% do post do nanodrop
- Mudei a interface do programa para que se pudesse ler e ver a simulação ao mesmo tempo

---
---
## Maio 2021
**Todo list geral**

- [ ] Criar post sobre Catenoide 
- [ ] Experimentos com bolhas de sabão
- [ ] Visualização e simulação de superfícies com o Surface Evolver 
  
---

**Sab**
: Estudar sobre cálculo variacional

- Fiz uma pesquisa de reconhecimento para saber como o cálculo de variações se aplicavam a outras áreas
- Resolvi o problema da geodésica em um plano

---

**Dom**

: Estudar superfícies mínimas

- Encontrei diversos vídeos interessantes sobre topologia e superfícies mínimas que se relacionavam com o cálculo da variações.
- Encontrei vídeo sobre experimento com bolhas em estruturas que modificavam a forma da superfície mínima.

---

### *Semana* **03/05/2021 - 10/05/2021**

**Seg**
: Apresentar conteúdo estudado para colegas do Fisex

Fiz uma apresentação básica e geral das informações que venho coletando sobre Cálculo Variacional e Minimização de Superfícies

---

**Quar**
: Estudar surface evolver

- rendi a utilizar alguns comandos 
- z dois exemplos de superfícies (cube and moud)

---

**Qui**
: Terminar post sobre tensão superficial

Acredito estar perto de concluir agora preciso de feedback do time fisex para que detalhes sejam ajustados.

---

### *Semana* **10/05/2021 - 17/05/2021**

**Seg**
: Ler tutorial do Ajax sobre Surface Evolver

Li o tutorial do Ajax, achei muito bom e até esclarece pontos que deveriam aparecer na documentação do surface de cara

---

**Quar**

Estudei através de um workshop que estáva nas referências do tutorial do Ajax

--- 


**Qui**

Fiz a correção no post do Nano Drop

---

**Sex**

- Ajustei as condições de contorno do lj-box(simulação da tela inicial do site)
- Li texto enviado por Julien no grupo sobre cálculo variacional aplicado a capilaridade


---

### *Semana* **10/05/2021 - 17/05/2021**

**Ter**

- Modifiquei UI da simulação nanodrop 

---

**Quar**

- Reunião/exibição do que estava fazendo durante a semana


---
**Qui**

- Coloquei alguns shortcuts no repositorio catenoide
- Instalei e testei o SE-FIT

**Dom**

- Criei um tutorial sobre git e gitlab básico 